﻿namespace IzyTimeControlFacturacion.Enums
{
    public enum InvoicingStatus
    {
        PreInvoiced = 0,
        Invoiced = 1
    }
}