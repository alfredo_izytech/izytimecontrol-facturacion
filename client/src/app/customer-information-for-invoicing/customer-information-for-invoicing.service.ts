import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Invoicing, InvoicingStatus } from '../core/models/invoicing.model';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomerInformationForInvoicingService {
  private readonly getAllInvoicings = `api/invoicings`;
  private readonly getCurrentInvoicings = `api/invoicings/CurrentInvoicings`;
  private readonly getDatabaseCompanyInfo = `api/invoicings/DatabaseCompanyInfo`;
  private readonly saveInvoicings = `api/invoicings/PostInvoicingMassive`;
  private readonly verifyIfMonthWasInvoiced = `api/invoicings/VerifyIfMonthWasInvoiced`;
  private readonly verifyIfMonthWasPreInvoiced = `api/invoicings/VerifyIfMonthWasPreInvoiced`;

  private httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };
  constructor(private http: HttpClient) {}

  getAll(): Observable<Invoicing[]> {
    return this.http.get<Invoicing[]>(this.getAllInvoicings);
  }

  getCurrent(status: InvoicingStatus): Observable<Invoicing[]> {
    return this.http.get<Invoicing[]>(this.getCurrentInvoicings + '?status=' + status);
  }

  getNewInvoicings(): Observable<Invoicing[]> {
    return this.http.get<Invoicing[]>(this.getDatabaseCompanyInfo);
  }

  getIfMonthWasInvoiced(): Observable<boolean> {
    return this.http.get<boolean>(this.verifyIfMonthWasInvoiced);
  }

  getIfMonthWasPreInvoiced(): Observable<boolean> {
    return this.http.get<boolean>(this.verifyIfMonthWasPreInvoiced);
  }

  savePreInvoicing(Invoicings: Invoicing[], Uf: number, Status: InvoicingStatus) {
    const params = {
      Invoicings,
      Status,
      Uf
     };

    return this.http.post<void>(this.saveInvoicings, params, this.httpOptions)
      .pipe(
        map(() => ({ result: true })),
        catchError((data) => of(
          {
            result: false,
            response: JSON.stringify(data.error)
          }
        ))
      );
  }

  saveInvoicing(Invoicings: Invoicing[], Status: InvoicingStatus) {
    const params = {
      Invoicings,
      Status,
     };

    return this.http.post<void>(this.saveInvoicings, params, this.httpOptions)
      .pipe(
        map(() => ({ result: true })),
        catchError((data) => of(
          {
            result: false,
            response: JSON.stringify(data.error)
          }
        ))
      );
  }

}
