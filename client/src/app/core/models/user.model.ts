export interface User {
    Id: number;
    FirstName: string;
    LastName: string;
    Email: string;
    Username: string;
    Token: string;
}

export interface Session {
    token: string;
    user: User;
}
