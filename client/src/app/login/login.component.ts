import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../core/service/auth.service';
import Swal from 'sweetalert2';
import { first } from 'rxjs/operators';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
    currentYear = new Date();
    error = '';
    formLogin: FormGroup;
    hide = true;
    inProcess = false;
    isLoginError = false;
    login: FormGroup;
    returnUrl: string;
    submitted = false;

    private toggleButton: any;
    private sidebarVisible: boolean;
    private nativeElement: Node;

    constructor(
        private authService: AuthService,
        private element: ElementRef,
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private title: Title
    ) {
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
        this.currentYear.getDate();

        if (this.authService.currentUserValue) {
            this.router.navigate(['/']);
        }
    }

    ngOnInit() {
        this.title.setTitle('Ingresar · Facturación de Izytimecontrol');

        this.formLogin = this.formBuilder.group({
            userName: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });

        const navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
        const body = document.getElementsByTagName('body')[0];
        body.classList.add('login-page');
        body.classList.add('off-canvas-sidebar');
        const card = document.getElementsByClassName('card')[0];
        setTimeout(() => {
            // after 1000 ms we add the class animated to the login/register card
            card.classList.remove('card-hidden');
        }, 700);

        this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
    }

    get loginButtonText(): string {
        if (this.inProcess) {
            return 'Ingresando';
        }

        return 'Ingresar';
    }

    OnSubmit() {
        if (this.formLogin.invalid) {
            return;
        }

        this.inProcess = true;
        const { userName, password } = this.formLogin.getRawValue();

        this.authService.login(userName, password)
        .pipe(first())
        .subscribe(
            (data) => {
                this.router.navigate(['/']);
            },
            () => {
                this.isLoginError = true;
                this.inProcess = false;

                Swal.fire({
                    title: 'Error',
                    icon: 'error',
                    text: 'Usuario o contraseña incorrectos',
                    showConfirmButton: false
                });
            }
        );
    }

    getErrorMessage() {
        return this.formLogin.get('userName').hasError('required') ? 'Nombre de Usuario es requerido' :
            this.formLogin.get('userName').hasError('email') ? 'Ingrese un Email válido' : '';
    }

    ngOnDestroy() {
        const body = document.getElementsByTagName('body')[0];
        body.classList.remove('login-page');
        body.classList.remove('off-canvas-sidebar');
    }
}
