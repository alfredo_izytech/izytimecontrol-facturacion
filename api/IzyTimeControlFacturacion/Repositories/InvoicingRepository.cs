﻿using IzyTimeControlFacturacion.Data;
using IzyTimeControlFacturacion.Enums;
using IzyTimeControlFacturacion.Models;
using IzyTimeControlFacturacion.Requests;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace IzyTimeControlFacturacion.Repositories
{
    
    public class InvoicingRepository : IDisposable
    {
        private IzyTimeControlFacturacionContext _db;

        public InvoicingRepository()
        {
            _db = new IzyTimeControlFacturacionContext();
        }

        public async Task PostInvoicingMassive(InvoicingRequest request)
        {
            try
            {
                var currentDate = DateTime.Now;
                var currentMont = currentDate.Month;
                var previousInvoicings = await _db.Invoicings.Where(i => i.InvoicingStatus == request.Status && i.CreatedAt.Month.Equals(currentMont)).ToListAsync();

                if(previousInvoicings.Count > 0)
                {
                    _db.Invoicings.RemoveRange(previousInvoicings);
                }

                foreach (var item in request.Invoicings)
                {
                    long value = 0;
                    long valuePerCasinoEmployee = 0;

                    if (request.Status == InvoicingStatus.Invoiced)
                    {
                        value = item.Invoiced;
                        valuePerCasinoEmployee = item.InvoicedByCasino;
                    }
                    else if (item.Type == InvoicingType.ByFixValue)
                    {
                        value = (long)Math.Round(this.fixValue(item.CompanyValue, request.Uf),0);

                        if (item.HasCasino)
                        {
                            valuePerCasinoEmployee = (long)Math.Round(this.fixValue(item.CompanyCasinoValue, request.Uf), 0);
                        }
                        else
                        {
                            valuePerCasinoEmployee = 0;
                        }
                        if (item.CompanyId == 27)
                        {
                            value = 1350000;
                        }
                    }
                    else if (item.Type == InvoicingType.ByActiveBranch)
                    {
                        if (item.CompanyId == 9) 
                        {
                            item.TotalActiveBranches = item.TotalActiveBranches - (item.TotalActiveBranches - 37) / 2; // DBS
                        }
                        else if (item.CompanyId == 12)
                        {
                            item.TotalActiveBranches = item.TotalActiveBranches - (item.TotalActiveBranches - 13) / 2; // Make up
                        }
                        else if (item.CompanyId == 18)
                        {
                            item.TotalActiveBranches = item.TotalActiveBranches - (item.TotalActiveBranches - 11) / 2; // Cory
                        }

                        value = (long)Math.Round(this.fixValuePerBranch(item.TotalActiveBranches, request.Uf), 0);
                        valuePerCasinoEmployee = item.HasCasino ? (long)Math.Round(this.fixValuePerBranch(item.TotalActiveBranches, request.Uf), 0) : 0;

                        if (item.CompanyId == 10)
                        {
                            value = value / 2; //DJ
                        }
                    }
                    else if (item.Type == InvoicingType.ByActiveEmployee)
                    {
                        value = (long)Math.Round(this.fixValuePerEmployee(item.CompanyValue, item.TotalActiveEmployees, request.Uf), 0);
                        valuePerCasinoEmployee = item.HasCasino ? (long)Math.Round(this.fixValuePerEmployee(item.CompanyValue, item.TotalActiveCasinoEmployees, request.Uf), 0) : 0;
                    }

                    Invoicing invoicing = new Invoicing
                    {
                        CreatedAt = DateTime.Now,
                        CompanyId = item.CompanyId,
                        InvoicingType = item.Type,
                        InvoicingStatus = request.Status,
                        TotalActiveBranches = item.TotalActiveBranches,
                        TotalActiveEmployees = item.TotalActiveEmployees,
                        TotalInactiveBranches = item.TotalInactiveBranches,
                        TotalInactiveEmployees = item.TotalInactiveEmployees,
                        TotalActiveCasinoEmployees = item.TotalActiveCasinoEmployees,
                        TotalInactiveCasinoEmployees = item.TotalInactiveCasinoEmployees,
                        Value = value,
                        ValuePerCasinoEmployee = valuePerCasinoEmployee
                    };

                    _db.Invoicings.Add(invoicing);
                    await _db.SaveChangesAsync();
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private double fixValue(double companyValue, double uf)
        {
            var fixValue = companyValue * uf;
            return fixValue;
        }

        private double fixValuePerBranch(long totalActiveBranch, double uf)
        {
            var fixValue = uf * totalActiveBranch;
            return fixValue;
        }

        private double fixValuePerEmployee(double companyValue, long totalActiveEmployees, double uf)
        {
            var fixValue = companyValue * uf * totalActiveEmployees;
            return fixValue;
        }

        public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }
    }
}