﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using IzyTimeControlFacturacion.Data;
using IzyTimeControlFacturacion.DTO;
using IzyTimeControlFacturacion.Models;
using IzyTimeControlFacturacion.Requests;
using IzyTimeControlFacturacion.Security;

namespace IzyTimeControlFacturacion.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/users")]
    public class UsersController : ApiController
    {
        private IzyTimeControlFacturacionContext db = new IzyTimeControlFacturacionContext();

        // GET: api/Users
        public async Task<IHttpActionResult> GetUsers()
        {
            return Ok(await db.Users.ToListAsync());
        }

        [HttpGet]
        [Route("ByEmailAndPassword")]
        public async Task<IHttpActionResult> GetUserByEmailAndPassword(string email, string password)
        {
            try
            {
                User user = await db.Users.Where(u => u.Email == email && u.Password == password).FirstOrDefaultAsync();
                return Ok(user);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("Authenticate")]
        public async Task<IHttpActionResult> Authenticate(LoginRequest request)
        {
            try
            {
                if(request == null)
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }

                User user = await db.Users.Where(u => u.Email == request.Username && u.Password == request.Password).FirstOrDefaultAsync();
                if(user != null)
                {
                    var token = TokenGenerator.GenerateTokenJwt(request.Username);
                    
                    UserDTO response = new UserDTO
                    {
                        Id = user.Id,
                        Username = user.UserName,
                        Email = user.Email,
                        Firstname = user.FirstName,
                        Lastname = user.LastName,
                        Token = token
                    };

                    return Ok(response);
                }
                else
                {
                    return BadRequest("Usuario o contraseña incorrectos");
                }
               
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET: api/Users/5
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> GetUser(long id)
        {
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        // PUT: api/Users/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUser(long id, User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.Id)
            {
                return BadRequest();
            }

            db.Entry(user).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Users
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> PostUser(User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Users.Add(user);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = user.Id }, user);
        }

        // DELETE: api/Users/5
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> DeleteUser(long id)
        {
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            db.Users.Remove(user);
            await db.SaveChangesAsync();

            return Ok(user);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(long id)
        {
            return db.Users.Count(e => e.Id == id) > 0;
        }
    }
}