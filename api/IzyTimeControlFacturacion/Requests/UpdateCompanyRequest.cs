﻿using IzyTimeControlFacturacion.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IzyTimeControlFacturacion.Requests
{
    public class UpdateCompanyRequest
    {
        [Required]
        public long Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public double Value { get; set; }
        [Required] 
        public DateTime ActivationDate { get; set; }
        [Required] 
        public CustomerType Type { get; set; }
        [Required]
        public bool HasCasino { get; set; }
        public double? ValueForCasinoEmployees { get; set; }
    }
}