﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using IzyTimeControlFacturacion.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace IzyTimeControlFacturacion.Models
{
    public class Company
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        public CustomerType CustomerType { get; set; }
        public DateTime ActivationDate { get; set; }
        public double? Value { get; set; }
        public bool HasCasino { get; set; }
        public double? ValueForCasinoEmployees { get; set; }
        public ICollection<CompanyDatabase> Databases { get; set; }
        public ICollection<Invoicing> Invoicings { get; set; }

    }
}