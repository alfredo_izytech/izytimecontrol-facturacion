﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IzyTimeControlFacturacion.Requests
{
    public class InsertCompanyDatabaseRequest
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public long CompanyId { get; set; }
        [Required]
        public long ServerId { get; set; }
    }
}