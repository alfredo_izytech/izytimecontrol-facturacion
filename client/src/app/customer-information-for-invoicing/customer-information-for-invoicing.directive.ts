import {
  ChangeDetectorRef,
  Directive,
  ElementRef,
  HostBinding,
  Input,
  OnInit,
  Inject,
  Renderer2,
  SimpleChanges,
  OnChanges } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { InvoicingType, Invoicing } from '../core/models/invoicing.model';

interface Options {
  icon: string;
  tooltip: string;
}

const DEFAULToptions: Options = {
  icon: '',
  tooltip: ''
};

@Directive({
  selector: '[appCustomerInformationForInvoicing]',
  exportAs: 'appCustomerInformationForInvoicing'
})
export class CustomerInformationForInvoicingDirective implements OnInit {

  private options = DEFAULToptions;
  private icon: HTMLElement;

  @Input('appCustomerInformationForInvoicing') item: any;
  @HostBinding('class') classes = 'btn btn-sm btn-round btn-icon-aw';
  constructor(
    private host: ElementRef,
    private renderer: Renderer2,
    @Inject(DOCUMENT) private document: Document,
    private changeDetector: ChangeDetectorRef) { }

  ngOnInit(): void {
    const icon = document.createElement('i');
    icon.className = 'material-icons';
    this.icon = icon;
    this.setIconDisplay(this.item);
  }

  get tooltip(): string {
    return this.options.tooltip;
  }

  private setIconDisplay(item: Invoicing): void {
    this.options = {...this.options, ...this.buildOptions(item.Type)};
    this.icon.innerText = this.options.icon;
    this.renderer.appendChild(this.host.nativeElement, this.icon);
    this.changeDetector.detectChanges();
  }

  private buildOptions(Status: InvoicingType): Options {
    let options: Options;
    if (Status === InvoicingType.ByActiveBranch) {
      options = {
        icon: 'business',
        tooltip: 'Por sucursal '
      };
    }
    if (Status === InvoicingType.ByActiveEmployee) {
      options = {
        icon: 'person_add',
        tooltip: 'Por empleado '
      };
    }
    if (Status === InvoicingType.SpecialBilling) {
      options = {
        icon: 'check',
        tooltip: 'Facturación especial '
      };
    }
    return options;
  }
}
