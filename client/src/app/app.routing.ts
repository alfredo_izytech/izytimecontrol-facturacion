import { Routes } from '@angular/router';
import { AuthGuard } from './core/helpers/auth.guard';
import { LoginComponent } from './login/login.component';
// tslint:disable-next-line: max-line-length
import { CustomerInformationForInvoicingComponent } from './customer-information-for-invoicing/customer-information-for-invoicing.component';
import { LayoutComponent } from './layout/layout.component';
import { ServerComponent } from './server/server.component';
import { CompanyComponent } from './company/company.component';
import { InvoicingComponent } from './invoicing/invoicing.component';


export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: 'customer-information-for-invoicing',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    component: LayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'customer-information-for-invoicing',
        component: CustomerInformationForInvoicingComponent
      },
      {
        path: 'companies',
        component: CompanyComponent
      },
      {
        path: 'servers',
        component: ServerComponent
      },
      {
        path: 'invoicings',
        component: InvoicingComponent
      }
    ]
  },
  { path: '**', redirectTo: '' }
];
