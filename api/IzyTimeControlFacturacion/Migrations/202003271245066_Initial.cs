﻿namespace IzyTimeControlFacturacion.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Company",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        CustomerType = c.Int(nullable: false),
                        ActivationDate = c.DateTime(nullable: false),
                        DatabaseName = c.String(),
                        ServerId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Invoicing",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CompanyId = c.Long(nullable: false),
                        InvoicingType = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        TotalActiveBranches = c.Long(nullable: false),
                        TotalActiveEmployees = c.Long(nullable: false),
                        TotalInactiveBranches = c.Long(nullable: false),
                        TotalInactiveEmployees = c.Long(nullable: false),
                        ValuePerEmployee = c.Long(nullable: false),
                        ValuePerBranch = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Server",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Datasource = c.String(),
                        Username = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Server");
            DropTable("dbo.Invoicing");
            DropTable("dbo.Company");
        }
    }
}
