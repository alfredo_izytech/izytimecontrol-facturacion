import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CompanyService } from '../core/service/company.service';
import { MatDialog } from '@angular/material';
import { UpdateCompanyFormComponent } from '../update-company-form/update-company-form.component';
import { Company } from '../core/models/company.model';
import { faPlus, faTimes, faDatabase, faUserTag } from '@fortawesome/free-solid-svg-icons';
import { ExcelService } from '../core/service/excel.service';
import { InvoicingType } from '../core/models/invoicing.model';
import { Title } from '@angular/platform-browser';
import { CreateCompanyComponent } from '../create-company/create-company.component';
import Swal from 'sweetalert2';
import { DatabaseFormComponent } from '../database-form/database-form.component';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MatTableDataSource<Company> = new MatTableDataSource<Company>();
  displayedColumns = ['name', 'activation_date', 'value', 'type', 'casino_value', 'actions'];
  faPlus = faPlus;
  faTimes = faTimes;
  faDatabase = faDatabase;
  faUserTag = faUserTag;

  constructor(private excel: ExcelService,
              private companyService: CompanyService,
              private title: Title,
              public dialog: MatDialog) {}

  ngOnInit() {
    this.title.setTitle('Clientes · Facturación de Izytimecontrol');

    this.companyService.getAll().subscribe((data: Company[]) => {
      this.dataSource.data  = data;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  exportAll() {
    const exportData = this.dataSource.data.map(item => {
        return {
            Empresa: item.Name,
            'Fecha de activacion': item.ActivationDate,
            Valor: item.Value,
            Tipo: item.Type === InvoicingType.ByActiveBranch ? 'Por sucursal' : 'Por empleado',
        };
    });

    this.excel.exportAsExcelFile(exportData, 'empresas');
  }

  openCreateCompanyDialog(): void {

    const dialogRef = this.dialog.open(CreateCompanyComponent, {
      width: '650px',
      minHeight: '200px',
      position: { top: '10px' }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.companyService.getAll().subscribe((data: Company[]) => {
        this.dataSource.data  = data;
        this.dataSource.paginator = this.paginator;
      });
    });
  }

  openUpdateCompanyDialog(company: Company): void {

    const dialogRef = this.dialog.open(UpdateCompanyFormComponent, {
      width: '650px',
      minHeight: '200px',
      position: { top: '10px' },
      data: { data: company }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.companyService.getAll().subscribe((data: Company[]) => {
        this.dataSource.data  = data;
        this.dataSource.paginator = this.paginator;
      });
    });
  }

  openUpdateOrCreateDatabaseCompanyDialog(companyId: number) {
    const dialogRef = this.dialog.open(DatabaseFormComponent, {
      width: '650px',
      minHeight: '200px',
      position: { top: '10px' },
      data: { CompanyId: companyId }
    });
  }

  openDeleteCompanyDialog(companyId: number) {
    Swal.fire({
      title: '¿Esta seguro que desea eliminar este registro?',
      icon: 'warning',
      showLoaderOnConfirm: true,
      showCancelButton: true,
      confirmButtonText: 'Si',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
        this.companyService.deleteCompany(companyId).subscribe((request) => {
          if (request.result) {

            this.companyService.getAll().subscribe((data: Company[]) => {
              this.dataSource.data  = data;
              this.dataSource.paginator = this.paginator;
            });

            Swal.fire(
              'Operación realizada con exito',
              'La empresa ha sido eliminada',
              'success'
            );
          }
        });
      }
    });
  }
}
