import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faUser as farUser } from '@fortawesome/free-regular-svg-icons';
import { faFileExcel as farFileExcel } from '@fortawesome/free-regular-svg-icons';
import { CustomerInformationForInvoicingService } from '../customer-information-for-invoicing/customer-information-for-invoicing.service';
import { Invoicing, InvoicingType } from '../core/models/invoicing.model';
import { ExcelService } from '../core/service/excel.service';
import { Title } from '@angular/platform-browser';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-invoicing',
  templateUrl: './invoicing.component.html',
  styleUrls: ['./invoicing.component.scss']
})
export class InvoicingComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MatTableDataSource<Invoicing> = new MatTableDataSource<Invoicing>();
  displayedColumns = [
    'invoicing_date',
    'company',
    'active_employee',
    'inactive_employee',
    'active_branch',
    'inactive_branch',
    'customer_type',
    'invoicing'
  ];

  faUser = faUser;
  farUser = farUser;
  farFileExcel = farFileExcel;
  noData = this.dataSource.connect().pipe(map(data => data.length === 0));

  constructor(private excel: ExcelService,
              private service: CustomerInformationForInvoicingService,
              private title: Title) {}

  ngOnInit() {
    this.title.setTitle('Historico · Facturación de Izytimecontrol');

    this.service.getAll().subscribe((dataInvoice: Invoicing[]) => {
      this.dataSource.data = dataInvoice;
      this.dataSource.paginator = this.paginator;
    });
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  exportAll() {
    const exportData = this.dataSource.data.map(item => {
        return {
            Empresa: item.CompanyName,
            'Empleados activos': item.TotalActiveEmployees,
            'Empleados inactivos': item.TotalInactiveEmployees,
            'Sucursales activas': item.TotalActiveBranches,
            'Sucursales inactivas': item.TotalInactiveBranches,
            'Tipo de facturación': item.Type === InvoicingType.ByActiveBranch ? 'Por sucursal' : 'Por empleado',
            Facturado: item.Invoiced
        };
    });
    this.excel.exportAsExcelFile(exportData, 'historico');
  }
}
