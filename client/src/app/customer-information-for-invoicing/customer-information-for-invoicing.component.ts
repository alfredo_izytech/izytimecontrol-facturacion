import { Component, OnInit, ViewChild, AfterViewInit, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { faUser, faFileInvoiceDollar, faCheck, faUserTag } from '@fortawesome/free-solid-svg-icons';
import { faUser as farUser, faFileExcel as farFileExcel } from '@fortawesome/free-regular-svg-icons';
import { CustomerInformationForInvoicingService } from './customer-information-for-invoicing.service';
import { Invoicing, InvoicingType, InvoicingStatus } from '../core/models/invoicing.model';
import { MatDialog } from '@angular/material';
import { CreateInvoicingComponent } from '../create-invoicing/create-invoicing.component';
import { ExcelService } from '../core/service/excel.service';
import { Title } from '@angular/platform-browser';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import Swal from 'sweetalert2';

pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-customer-information-for-invoicing',
  templateUrl: './customer-information-for-invoicing.component.html',
  styleUrls: ['./customer-information-for-invoicing.component.scss']
})
export class CustomerInformationForInvoicingComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  data: Invoicing[];
  dataSource: MatTableDataSource<Invoicing> = new MatTableDataSource<Invoicing>();
  displayedColumns = [
    'company',
    'active_employee',
    'inactive_employee',
    'active_branch',
    'inactive_branch',
    'active_casino_employees',
    'customer_type',
    'invoicing',
    'casino_invoicing',
    'options'
  ];
  disableInvoicingButton = true;
  faCheck = faCheck;
  farFileExcel = farFileExcel;
  faFileInvoiceDollar = faFileInvoiceDollar;
  faUser = faUser;
  farUser = farUser;
  faUserTag = faUserTag;
  invoicedMonth: boolean;
  preInvoicedMonth: boolean;
  loaded = false;
  status: InvoicingStatus;

  constructor(private excel: ExcelService,
              private service: CustomerInformationForInvoicingService,
              private title: Title,
              public dialog: MatDialog) {
  }

  ngOnInit() {
    this.title.setTitle('Facturación · Facturación de Izytimecontrol');

    Swal.fire({
      title: 'Cargando',
      text: `Espere un momento...`,
      icon: 'info',
      showConfirmButton: false
    });

    this.service.getIfMonthWasInvoiced().subscribe((invoicedMonth: boolean) => {
      this.invoicedMonth = invoicedMonth;

      this.service.getIfMonthWasPreInvoiced().subscribe((preInvoicedMonth: boolean) => {
        this.preInvoicedMonth = preInvoicedMonth;
        this.status = this.invoicedMonth ? InvoicingStatus.Invoiced : InvoicingStatus.PreInvoiced;

        if (!this.preInvoicedMonth) {
          this.disableInvoicingButton = true;
        } else if (this.preInvoicedMonth && !this.invoicedMonth) {
          this.disableInvoicingButton = false;
        } else {
          this.disableInvoicingButton = true;
        }

        if (this.invoicedMonth || this.preInvoicedMonth) {
          this.service.getCurrent(this.status).subscribe((dataInvoice: Invoicing[]) => {
            this.data = dataInvoice;
            this.dataSource.data = dataInvoice;
            this.dataSource.paginator = this.paginator;
            this.loaded = true;

            Swal.close();
          });
        } else {
          this.service.getNewInvoicings().subscribe((dataInvoice: Invoicing[]) => {
            this.data = dataInvoice;
            this.dataSource.data = dataInvoice;
            this.dataSource.paginator = this.paginator;
            this.loaded = true;

            Swal.close();
          });
        }
      });
    });
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  exportAll() {

    let exportData: any[] = [];
    let type = '';
    this.dataSource.data.forEach(item => {

      if (item.Type === InvoicingType.ByActiveBranch) {
        type = 'Por sucursal';
      } else if (item.Type === InvoicingType.ByActiveEmployee) {
        type = 'Por empleado';
      } else if (item.Type === InvoicingType.ByFixValue) {
        type = 'Por valor fijo';
      } else {
        type = 'Facturación especial';
      }

      exportData.push(
        {
          Empresa: item.CompanyName,
          'Empleados activos': item.TotalActiveEmployees,
          'Empleados inactivos': item.TotalInactiveEmployees,
          'Sucursales activas': item.TotalActiveBranches,
          'Sucursales inactivas': item.TotalInactiveBranches,
          'Tipo de facturación': type,
          Facturado: item.Invoiced
      });

      if (item.HasCasino) {
        exportData.push(
          {
            Empresa: item.CompanyName + ' Casino',
            'Empleados activos': item.TotalActiveCasinoEmployees,
            'Empleados inactivos': item.TotalInactiveCasinoEmployees,
            'Sucursales activas': '',
            'Sucursales inactivas': '',
            'Tipo de facturación': type,
            Facturado: item.InvoicedByCasino
        });
      }

      return exportData;
    });

    this.excel.exportAsExcelFile(exportData, 'facturacion');
  }

  generatePdf(row: Invoicing) {
    const documentDefinition = this.getDocumentDefinition(row);

    pdfMake.createPdf(documentDefinition).open();
  }

  getDocumentDefinition(row: Invoicing) {

    let customerType;

    switch (row.Type) {
      case InvoicingType.ByActiveBranch:
        customerType = 'Por sucursales activas';
        break;
      case InvoicingType.ByActiveEmployee:
        customerType = 'Por empleados activos';
        break;
      case InvoicingType.SpecialBilling:
        customerType = 'Facturación especial';
        break;
    }

    return {
      content: [
        {
          text: row.CompanyName,
          bold: true,
          fontSize: 20,
          alignment: 'center',
          margin: [0, 0, 0, 20]
        },
        {
          columns: [
            [
              {
                text: 'Tipo de facturación: ' + customerType
              },
              {
                text: 'Monto facturado: ' + row.Invoiced
              }
            ],
          ],
          margin: [0, 0, 0, 20]
        },
        {
          text: 'Información de facturación',
          style: 'header',
          bold: true,
          fontSize: 20,
          alignment: 'center',
          margin: [0, 0, 0, 20]
        },
        this.getInvoicingObject(row)
      ],
    };
  }

  getInvoicingObject(row: Invoicing) {
    return {
      table: {
        widths: ['*', '*', '*', '*'],
        body: [
          [
            {
            text: 'Empleados activos',
            style: 'tableHeader'
            },
            {
              text: 'Empleados inactivos',
              style: 'tableHeader'
            },
            {
              text: 'Sucursales activa',
              style: 'tableHeader'
            },
            {
              text: 'Sucursales inactivas',
              style: 'tableHeader'
            }
          ],
          [
            {
              text: row.TotalActiveEmployees
            },
            {
              text: row.TotalInactiveEmployees
            },
            {
              text: row.TotalActiveBranches
            },
            {
              text: row.TotalInactiveBranches
            },
          ]
        ]
      }
    };
  }

  openDialog(): void {

    const dialogRef = this.dialog.open(CreateInvoicingComponent, {
      width: '650px',
      minHeight: '200px',
      position: { top: '10px' },
      data: { data: this.data }
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.preInvoicedMonth = true;
        this.disableInvoicingButton = false;
        this.service.getCurrent(InvoicingStatus.PreInvoiced).subscribe((data: Invoicing[]) => {
          this.data = data;
          this.dataSource.data = data;
          this.dataSource.paginator = this.paginator;
        });
      }
    });
  }

  saveInvoicings() {
    this.service.saveInvoicing(this.dataSource.data, InvoicingStatus.Invoiced).subscribe((response) => {
      Swal.close();

      if (response.result) {
        this.disableInvoicingButton = true;
        this.invoicedMonth = true;

        Swal.fire({
          title: 'Operación exitosa',
          icon: 'success',
          showConfirmButton: false,
          timer: 3000
        });

      } else {
        Swal.fire({
          title: 'No se pudo realizar la operación',
          icon: 'error',
          showConfirmButton: false,
          timer: 3000
        });
      }
    });
  }

}
