export interface CreateDatabaseForm {
    CompanyId: number;
    DatabaseName: string;
    ServerId: number;
}

export interface UpdateDatabaseForm {
    Id: number;
    CompanyId: number;
    DatabaseName: string;
    ServerId: number;
}

export enum DatabaseFormType {
    Create = 1,
    Update = 2
}
