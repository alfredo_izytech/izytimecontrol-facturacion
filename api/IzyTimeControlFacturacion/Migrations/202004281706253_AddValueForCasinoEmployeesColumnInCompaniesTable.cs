﻿namespace IzyTimeControlFacturacion.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddValueForCasinoEmployeesColumnInCompaniesTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "ValueForCasinoEmployees", c => c.Double());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "ValueForCasinoEmployees");
        }
    }
}
