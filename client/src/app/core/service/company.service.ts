import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { UpdateCompanyForm, CreateCompanyForm } from '../models/company.model';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  private readonly getCompanies = `api/companies`;

  private httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) {}

  getAll(): Observable<any[]> {
    return this.http.get<any[]>(this.getCompanies);
  }

  createCompany(company: CreateCompanyForm) {
    return this.http.post<void>(this.getCompanies, company, this.httpOptions).pipe(
      map(() => ({ result: true })),
      catchError((data) => of(
        {
          result: false,
          response: JSON.stringify(data.error)
        }
      ))
    );
  }

  deleteCompany(companyId: number) {
    return this.http.delete<void>(this.getCompanies + '/' + companyId, this.httpOptions).pipe(
      map(() => ({ result: true })),
      catchError((data) => of(
        {
          result: false,
          response: JSON.stringify(data.error)
        }
      ))
    );
  }

  updateCompany(company: UpdateCompanyForm) {
    return this.http.put<void>(this.getCompanies + '/' + company.Id, company, this.httpOptions).pipe(
      map(() => ({ result: true })),
      catchError((data) => of(
        {
          result: false,
          response: JSON.stringify(data.error)
        }
      ))
    );
  }

}
