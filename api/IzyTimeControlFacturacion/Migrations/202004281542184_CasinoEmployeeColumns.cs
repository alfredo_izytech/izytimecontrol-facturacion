﻿namespace IzyTimeControlFacturacion.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CasinoEmployeeColumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "HasCasino", c => c.Boolean(nullable: false, defaultValue: false));
            AddColumn("dbo.Invoicings", "TotalActiveCasinoEmployees", c => c.Long(nullable: false));
            AddColumn("dbo.Invoicings", "TotalInactiveCasinoEmployees", c => c.Long(nullable: false));
            AddColumn("dbo.Invoicings", "ValuePerCasinoEmployee", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Invoicings", "ValuePerCasinoEmployee");
            DropColumn("dbo.Invoicings", "TotalInactiveCasinoEmployees");
            DropColumn("dbo.Invoicings", "TotalActiveCasinoEmployees");
            DropColumn("dbo.Companies", "HasCasino");
        }
    }
}
