﻿namespace IzyTimeControlFacturacion.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyValueColumnDataTypeInCompanyTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Companies", "Value", c => c.Double());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Companies", "Value", c => c.Long(nullable: false));
        }
    }
}
