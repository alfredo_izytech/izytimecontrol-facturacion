﻿namespace IzyTimeControlFacturacion.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PendingChanges : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Databases", newName: "CompanyDatabases");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.CompanyDatabases", newName: "Databases");
        }
    }
}
