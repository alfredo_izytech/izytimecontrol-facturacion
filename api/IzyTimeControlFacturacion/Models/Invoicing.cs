﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using IzyTimeControlFacturacion.Enums;

namespace IzyTimeControlFacturacion.Models
{
    public class Invoicing
    {
        [Key]
        public long Id { get; set; }
        public long CompanyId { get; set; }
        public InvoicingType InvoicingType { get; set; }
        public InvoicingStatus InvoicingStatus { get; set; }
        public DateTime CreatedAt { get; set; }
        public long TotalActiveBranches { get; set; }
        public long TotalActiveEmployees { get; set; }
        public long TotalInactiveBranches { get; set; }
        public long TotalInactiveEmployees { get; set; }
        public long TotalActiveCasinoEmployees { get; set; }
        public long TotalInactiveCasinoEmployees { get; set; }
        public long ValuePerCasinoEmployee { get; set; }
        public long Value { get; set; }
        public virtual Company Company { get; set; }
    }
}