﻿using IzyTimeControlFacturacion.Enums;
using System;

namespace IzyTimeControlFacturacion.DTO
{
    public class CompanyDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public double? Value { get; set; }
        public string ValueWithFormat { get; set; }
        public double? ValueForCasinoEmployees { get; set; }
        public string ValueForCasinoEmployeesWithFormat { get; set; }
        public CustomerType Type { get; set; }
        public DateTime ActivationDate { get; set; }
        public bool HasCasino { get; set; }

    }
}