﻿namespace IzyTimeControlFacturacion.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DropServerIdColumnCompanyTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Companies", "ServerId", "dbo.Servers");
            DropIndex("dbo.Companies", new[] { "ServerId" });
            DropColumn("dbo.Companies", "ServerId");
            DropColumn("dbo.Companies", "DatabaseName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Companies", "DatabaseName", c => c.String());
            AddColumn("dbo.Companies", "ServerId", c => c.Long(nullable: false));
            CreateIndex("dbo.Companies", "ServerId");
            AddForeignKey("dbo.Companies", "ServerId", "dbo.Servers", "Id", cascadeDelete: true);
        }
    }
}
