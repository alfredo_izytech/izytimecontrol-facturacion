﻿namespace IzyTimeControlFacturacion.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InvoicingValueColumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Invoicings", "Value", c => c.Long(nullable: false));
            DropColumn("dbo.Invoicings", "ValuePerEmployee");
            DropColumn("dbo.Invoicings", "ValuePerBranch");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Invoicings", "ValuePerBranch", c => c.Long(nullable: false));
            AddColumn("dbo.Invoicings", "ValuePerEmployee", c => c.Long(nullable: false));
            DropColumn("dbo.Invoicings", "Value");
        }
    }
}
