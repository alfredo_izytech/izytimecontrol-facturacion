﻿namespace IzyTimeControlFacturacion.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PendingRelationships : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Company", newName: "Companies");
            RenameTable(name: "dbo.Invoicing", newName: "Invoicings");
            RenameTable(name: "dbo.Server", newName: "Servers");
            CreateIndex("dbo.Invoicings", "CompanyId");
            AddForeignKey("dbo.Invoicings", "CompanyId", "dbo.Companies", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Invoicings", "CompanyId", "dbo.Companies");
            DropIndex("dbo.Invoicings", new[] { "CompanyId" });
            RenameTable(name: "dbo.Servers", newName: "Server");
            RenameTable(name: "dbo.Invoicings", newName: "Invoicing");
            RenameTable(name: "dbo.Companies", newName: "Company");
        }
    }
}
