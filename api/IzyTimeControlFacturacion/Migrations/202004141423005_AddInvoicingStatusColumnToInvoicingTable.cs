﻿namespace IzyTimeControlFacturacion.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddInvoicingStatusColumnToInvoicingTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Invoicings", "InvoicingStatus", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Invoicings", "InvoicingStatus");
        }
    }
}
