﻿namespace IzyTimeControlFacturacion.Migrations
{
    using IzyTimeControlFacturacion.Enums;
    using IzyTimeControlFacturacion.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<IzyTimeControlFacturacion.Data.IzyTimeControlFacturacionContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(IzyTimeControlFacturacion.Data.IzyTimeControlFacturacionContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
            //var users = new List<User>
            //{
            //   new User { Id= 1, Email = "alfredo@izytech.cl", FirstName = "Carlos", LastName = "Castillo", UserName = "alfredo@izytech.cl", Password = "@Mipassword123" }
            //};

            //users.ForEach(s => context.Users.AddOrUpdate(s));
            //context.SaveChanges();

            //var servers = new List<Server>
            //{
            //   new Server { Id = 1, Datasource = "izy2016-01.cmrnq8v2kv0c.us-east-1.rds.amazonaws.com", Name = "Server 1", Username = "db_izymarketing", Password = "Izymarketing_2016" },
            //   new Server { Id = 2, Datasource = "izy2016-02.cmrnq8v2kv0c.us-east-1.rds.amazonaws.com", Name = "Server 2", Username = "db_izymarketing2", Password = "IzyTimeControl22018" },
            //   new Server { Id = 3, Datasource = "izy2019-03.cmrnq8v2kv0c.us-east-1.rds.amazonaws.com", Name = "Server 3", Username = "db_izymarketing3", Password = "IzyTimeControl22019" },
            //};

            //servers.ForEach(s => context.Servers.AddOrUpdate(s));
            //context.SaveChanges();

            var companies = new List<Company>
            {
               new Company
               {
                   Id = 1,
                   Name ="Izytech",
                   CustomerType = CustomerType.ByActiveEmployee,
                   ActivationDate = DateTime.Now,
                   Value = 2,
                   Databases = new List<CompanyDatabase>()
                   {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Demo",
                           ServerId = 3,
                      }
                  }
               },
               new Company
               {
                  Id = 2,
                  Name = "Casino Agusta",
                  CustomerType = CustomerType.ByActiveEmployee,
                  ActivationDate = DateTime.Now,
                  Value = 2,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_CasinoAgunsa",
                           ServerId = 3,
                      }
                  }
              },
              new Company
              {
                  Id = 3,
                  Name = "Equifax",
                  CustomerType = CustomerType.ByActiveEmployee,
                  ActivationDate = DateTime.Now,
                  Value = 4,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Equifax",
                           ServerId = 3,
                      }
                  }
              },
              new Company
              {
                  Id = 4,
                  Name = "Valladolid",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Value = 5000,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Valladolid",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 5,
                  Name = "UCM",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Value = 5000,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_UCM",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 6,
                  Name = "UCM Medico",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Value = 5000,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_UCM_Medico",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 7,
                  Name = "Privilege",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Value = 5000,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Bercovich",
                           ServerId = 1,
                      }
                  }
              },
              new Company
              {
                  Id = 8,
                  Name = "Colada",
                  CustomerType = CustomerType.ByFixValue,
                  ActivationDate = DateTime.Now,
                  Value = 2.36,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Colada",
                           ServerId = 1,
                      }
                  }
              },
              new Company
              {
                  Id = 9,
                  Name = "DBS",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Dbs",
                           ServerId = 1,
                      }
                  }
              },
              new Company
              {
                  Id = 10,
                  Name = "DJ",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_DBS_DJ",
                           ServerId = 1,
                      }
                  }
              },
              new Company
              {
                  Id = 11,
                  Name = "Karmac",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Value = 5000,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Karmac",
                           ServerId = 1,
                      }
                  }
              },
              new Company
              {
                  Id = 12,
                  Name = "Make UP",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Value = 5000,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_MakeUP",
                           ServerId = 1,
                      }
                  }
              },
              new Company
              {
                  Id = 13,
                  Name = "ITC_Mateos",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Value = 4,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Mateos",
                           ServerId = 1,
                      }
                  }
              },
              new Company
              {
                  Id = 14,
                  Name = "Niu",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Value = 1.1,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Niu",
                           ServerId = 1,
                      }
                  }
              },
              new Company
              {
                  Id = 15,
                  Name = "Pasteur",
                  CustomerType = CustomerType.ByFixValue,
                  ActivationDate = DateTime.Now,
                  Value = 8,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Pasteur",
                           ServerId = 1,
                      }
                  }
              },
              new Company
              {
                  Id = 16,
                  Name = "Patio Bellavista",
                  CustomerType = CustomerType.ByFixValue,
                  ActivationDate = DateTime.Now,
                  Value = 5,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_PatioBellavista",
                           ServerId = 1,
                      }
                  }
              },
               new Company
              {
                  Id = 17,
                  Name = "Rambrands",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Value = 5000,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Rambrands",
                           ServerId = 1,
                      }
                  }
              },
              new Company
              {
                  Id = 18,
                  Name = "Rheem",
                  CustomerType = CustomerType.ByActiveEmployee,
                  ActivationDate = DateTime.Now,
                  Value = 0.03,
                  HasCasino = true,
                  ValueForCasinoEmployees = 0.03,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Rheem",
                           ServerId = 1,
                      }
                  }
              },
              new Company
              {
                  Id = 19,
                  Name = "Tuves",
                  CustomerType = CustomerType.ByFixValue,
                  ActivationDate = DateTime.Now,
                  Value = 7.5,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Tuves",
                           ServerId = 1,
                      }
                  }
              },
              new Company
              {
                  Id = 20,
                  Name = "AssistCard",
                  CustomerType = CustomerType.ByFixValue,
                  ActivationDate = DateTime.Now,
                  Value = 7,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_AssistCard",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 21,
                  Name = "Belcorp",
                  CustomerType = CustomerType.ByActiveEmployee,
                  ActivationDate = DateTime.Now,
                  Value = 5000,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Belcorp",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 22,
                  Name = "ITC_Buro",
                  CustomerType = CustomerType.ByFixValue,
                  ActivationDate = DateTime.Now,
                  Value = 5000,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Buro",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 23,
                  Name = "Cassis",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Value = 5000,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Cassis",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 24,
                  Name = "CEPECH",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Value = 0.04,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Cepech_Admin",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 25,
                  Name = "Caren",
                  CustomerType = CustomerType.ByFixValue,
                  ActivationDate = DateTime.Now,
                  Value = 15,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_ControlTopCaren",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 26,
                  Name = "Colegio Suizo",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Value = 10,
                  HasCasino = true,
                  ValueForCasinoEmployees = 2,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_CSS",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 27,
                  Name = "Dimerc",
                  CustomerType = CustomerType.ByFixValue,
                  ActivationDate = DateTime.Now,
                  Value = 1350000,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_DimercBeta",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 28,
                  Name = "Dominos Pizza",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Value = 5000,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_DominosPizza",
                           ServerId = 2,
                      }
                  }
              },
               new Company
              {
                  Id = 29,
                  Name = "Everis",
                  CustomerType = CustomerType.ByFixValue,
                  ActivationDate = DateTime.Now,
                  Value = 8,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Everis",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 30,
                  Name = "IPChile",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Value = 5000,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_IPChile",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 31,
                  Name = "ISA",
                  CustomerType = CustomerType.ByFixValue,
                  ActivationDate = DateTime.Now,
                  Value = 10,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_ISA",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 32,
                  Name = "Johnny Rockets",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Value = 5000,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_JohnnyRockets",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 33,
                  Name = "Konecta",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Value = 5000,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Konecta",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 34,
                  Name = "Marienberg",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Value = 5000,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Marienberg",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 35,
                  Name = "Nh Hoteles",
                  CustomerType = CustomerType.ByFixValue,
                  ActivationDate = DateTime.Now,
                  Value = 3.5,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_NhHoteles",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 36,
                  Name = "Pedro de Valdivia",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Value = 5000,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_PedroDeValdivia",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 37,
                  Name = "Protorq",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Value = 6,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Protorq",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 38,
                  Name = "Punta blanca",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Value = 5000,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_PuntaBlanca",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 39,
                  Name = "Renaissance Hotels",
                  CustomerType = CustomerType.ByFixValue,
                  ActivationDate = DateTime.Now,
                  Value = 3.62,
                  HasCasino = true,
                  ValueForCasinoEmployees = 2,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_RenaissanceHotels",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 40,
                  Name = "Doctor Simi",
                  CustomerType = CustomerType.ByFixValue,
                  ActivationDate = DateTime.Now,
                  Value = 5,
                  HasCasino = true,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Simi",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 41,
                  Name = "Socoarte",
                  CustomerType = CustomerType.ByFixValue,
                  ActivationDate = DateTime.Now,
                  Value = 16,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Socoarte",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 42,
                  Name = "Tawa",
                  CustomerType = CustomerType.ByFixValue,
                  ActivationDate = DateTime.Now,
                  Value = 48.9,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_Tawa",
                           ServerId = 2,
                      }
                  }
              },
              new Company
              {
                  Id = 43,
                  Name = "Niu Chicureo",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Value = 5000,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_NIU",
                           ServerId = 1,
                      }
                  }
              },
              new Company
              {
                  Id = 44,
                  Name = "Cory",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "Cory",
                           ServerId = 1,
                      }
                  }
              },

              new Company
              {
                  Id = 45,
                  Name = "Palumbo",
                  CustomerType = CustomerType.ByActiveBranch,
                  ActivationDate = DateTime.Now,
                  Value = 0.055,
                  Databases = new List<CompanyDatabase>()
                  {
                      new CompanyDatabase
                      {
                           Name =  "ITC_PeluqueriasIntegrales",
                           ServerId = 2,
                      }
                  }
              }
            };

            companies.ForEach(c => context.Companies.AddOrUpdate(c));
            context.SaveChanges();
        }
    }
}
