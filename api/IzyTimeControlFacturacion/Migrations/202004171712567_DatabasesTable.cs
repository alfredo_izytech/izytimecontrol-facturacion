﻿namespace IzyTimeControlFacturacion.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DatabasesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Databases",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        CompanyId = c.Long(nullable: false),
                        ServerId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.CompanyId)
                .Index(t => t.ServerId);

            AddForeignKey("dbo.Databases", "ServerId", "dbo.Servers", "Id", cascadeDelete: true);

        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Databases", "ServerId", "dbo.Servers");
            DropIndex("dbo.Databases", new[] { "ServerId" });
            DropTable("dbo.Databases");
        }
    }
}
