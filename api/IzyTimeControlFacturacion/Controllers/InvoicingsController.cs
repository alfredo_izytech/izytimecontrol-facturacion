﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using IzyTimeControlFacturacion.Data;
using IzyTimeControlFacturacion.DTO;
using IzyTimeControlFacturacion.Enums;
using IzyTimeControlFacturacion.Models;
using IzyTimeControlFacturacion.Repositories;
using IzyTimeControlFacturacion.Requests;

namespace IzyTimeControlFacturacion.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/invoicings")]
    public class InvoicingsController : ApiController
    {
        private IzyTimeControlFacturacionContext db = new IzyTimeControlFacturacionContext();
        private InvoicingRepository invoicingRepository = new InvoicingRepository();

        // GET: api/Invoicings
        public async Task<IHttpActionResult> GetInvoicings()
        {
            try
            {
                var invoicings = await db.Invoicings
                    .Include(b => b.Company)
                    .Where(i => i.InvoicingStatus == InvoicingStatus.Invoiced)
                    .ToListAsync();
                var invoicingsInfo = new List<InvoicingDTO>();

                foreach (var invoicing in invoicings)
                {
                    var invoicingInfo = new InvoicingDTO();

                    invoicingInfo.CompanyId = invoicing.CompanyId;
                    invoicingInfo.CompanyName = invoicing.Company.Name;
                    invoicingInfo.CompanyValue = invoicing.Company.Value == null ? 0 : invoicing.Company.Value;
                    invoicingInfo.CompanyCasinoValue = invoicing.Company.ValueForCasinoEmployees == null ? 0 : invoicing.Company.ValueForCasinoEmployees;
                    invoicingInfo.CreatedAt = invoicing.CreatedAt.ToString("dd/MM/yyyy");
                    invoicingInfo.Type = invoicing.Company.CustomerType;
                    invoicingInfo.HasCasino = invoicing.Company.HasCasino;
                    invoicingInfo.TotalActiveEmployees = invoicing.TotalActiveEmployees;
                    invoicingInfo.TotalInactiveEmployees = invoicing.TotalInactiveEmployees;
                    invoicingInfo.TotalActiveBranches = invoicing.TotalActiveBranches;
                    invoicingInfo.TotalInactiveBranches = invoicing.TotalActiveEmployees;
                    invoicingInfo.TotalActiveCasinoEmployees = invoicing.TotalActiveCasinoEmployees;
                    invoicingInfo.TotalInactiveCasinoEmployees = invoicing.TotalActiveCasinoEmployees;
                    invoicingInfo.Invoiced = invoicing.Value;
                    invoicingInfo.InvoicedByCasino = invoicing.ValuePerCasinoEmployee;
                    invoicingInfo.InvoicedWithFormat = ("$ " + string.Format("{0:#,0}", invoicing.Value)).Replace(',', '.');
                    invoicingInfo.InvoicedByCasinoWithFormat = ("$ " + string.Format("{0:#,0}", invoicing.ValuePerCasinoEmployee)).Replace(',', '.');
                    invoicingsInfo.Add(invoicingInfo);
                }

                return Ok(invoicingsInfo);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("DatabaseCompanyInfo")]
        public async Task<IHttpActionResult> GetDatabaseCompanyInfo()
        {
            try
            {
                var data = from databases in db.CompanyDatabases
                           join companies in db.Companies on databases.CompanyId equals companies.Id
                           join servers in db.Servers on databases.ServerId equals servers.Id
                           select new InvoicingDTO
                           {
                               CompanyId = companies.Id,
                               CompanyName = companies.Name,
                               CompanyValue = companies.Value == null ? 0 : companies.Value,
                               CompanyCasinoValue = companies.ValueForCasinoEmployees == null ? 0 : companies.ValueForCasinoEmployees,
                               Type = companies.CustomerType,
                               ServerName = servers.Name,
                               Datasource = servers.Datasource,
                               DatabaseName = databases.Name,
                               Username = servers.Username,
                               Password = servers.Password,
                               HasCasino = companies.HasCasino,
                               
                           };

                List<InvoicingDTO> databaseCompanies = await data.ToListAsync();

                foreach(var item in databaseCompanies)
                {
                    using (SqlConnection connection = new SqlConnection("Data Source=" + item.Datasource + ";Initial Catalog=" + item.DatabaseName + ";Persist Security Info=True;User ID=" + item.Username + ";Password=" + item.Password + ";"))
                    {

                        string activeEmployeesQuery = $"SELECT Count(*) FROM Employees as e WHERE e.IsActive = 1 AND e.Type < 4";
                        string nonActiveEmployeesQuery = $"SELECT Count(*) FROM Employees as e WHERE IsActive = 0 AND e.Type < 4";
                        string activeBranchesQuery = $"SELECT Count(*) FROM BranchOffices WHERE IsActive = 1";
                        string nonActiveBranchesQuery = $"SELECT Count(*) FROM BranchOffices WHERE IsActive = 0";

                        if (item.CompanyId == 14)
                        {
                            activeEmployeesQuery = $"SELECT Count(*) FROM Employees as e WHERE e.IsActive = 1 AND e.Type < 4 AND e.BranchOriginId != 14;";
                            nonActiveEmployeesQuery = $"SELECT Count(*) FROM Employees as e WHERE e.IsActive = 0 AND e.Type < 4 AND e.BranchOriginId != 14;";
                            activeBranchesQuery = $"SELECT COUNT(*) FROM BranchOffices as b WHERE b.IsActive = 1 AND b.Id != 14";
                            nonActiveBranchesQuery = $"SELECT COUNT(*) FROM BranchOffices as b WHERE b.IsActive = 0 AND b.Id != 14";
                        }

                        if (item.CompanyId == 43)
                        {
                            activeBranchesQuery = $"SELECT COUNT(*) FROM BranchOffices as b WHERE b.IsActive = 1 AND b.Id = 14";
                            nonActiveBranchesQuery = $"SELECT COUNT(*) FROM BranchOffices as b WHERE b.IsActive = 0 AND b.Id = 14";
                            activeEmployeesQuery = $"SELECT Count(*) FROM Employees as e WHERE e.IsActive = 1 AND e.Type < 4 AND e.BranchOriginId = 14;";
                            nonActiveEmployeesQuery = $"SELECT Count(*) FROM Employees as e WHERE e.IsActive = 0 AND e.Type < 4 AND e.BranchOriginId = 14";
                        }

                        SqlCommand commandActiveEmployees = new SqlCommand(activeEmployeesQuery, connection);
                        SqlCommand commandNotActiveEmployees = new SqlCommand(nonActiveEmployeesQuery, connection);
                        SqlCommand commandActiveBranches = new SqlCommand(activeBranchesQuery, connection);
                        SqlCommand commandNotActiveBranches = new SqlCommand(nonActiveBranchesQuery, connection);
                       

                        await connection.OpenAsync();

                        var activeEmployees = (Int32)commandActiveEmployees.ExecuteScalar();
                        var InactiveEmployees = (Int32)commandNotActiveEmployees.ExecuteScalar();
                        var activeBranches = (Int32)commandActiveBranches.ExecuteScalar();
                        var inactiveBranches = (Int32)commandNotActiveBranches.ExecuteScalar();

                        if (item.HasCasino)
                        {
                            string activeCasinoEmployeesQuery = $"select COUNT(*) from Employees as e where e.Type = 4 and e.IsActive = 1";
                            string inactiveCasinoEmployeesQuery = $"select COUNT(*) from Employees as e where e.Type = 4 and e.IsActive = 0";

                            SqlCommand commandActiveCasinoEmployees = new SqlCommand(activeCasinoEmployeesQuery, connection);
                            SqlCommand commandNotActiveCasinoEmployees = new SqlCommand(inactiveCasinoEmployeesQuery, connection);

                            var activeCasinoEmployees = (Int32)commandActiveCasinoEmployees.ExecuteScalar();
                            var inactiveCasinoEmployees = (Int32)commandNotActiveCasinoEmployees.ExecuteScalar();

                            item.TotalActiveCasinoEmployees = activeCasinoEmployees;
                            item.TotalInactiveCasinoEmployees = inactiveCasinoEmployees;
                        }
                        else 
                        {
                            item.TotalInactiveCasinoEmployees = 0;
                            item.TotalActiveCasinoEmployees = 0;
                        }

                        item.TotalActiveEmployees = activeEmployees;
                        item.TotalInactiveEmployees = InactiveEmployees;
                        item.TotalActiveBranches = activeBranches;
                        item.TotalInactiveBranches = inactiveBranches;
                    }
                }

                return Ok(databaseCompanies);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
            
        }

        // GET: api/Invoicings/5
        [ResponseType(typeof(Invoicing))]
        public async Task<IHttpActionResult> GetInvoicing(long id)
        {
            Invoicing invoicing = await db.Invoicings.FindAsync(id);
            if (invoicing == null)
            {
                return NotFound();
            }

            return Ok(invoicing);
        }

        [HttpGet]
        [Route("CurrentInvoicings")]
        public async Task<IHttpActionResult> GetCurrentInvoicings(InvoicingStatus status)
        {
            try
            {
                var currentDate = DateTime.Now;
                var currentMont = currentDate.Month;
                var invoicings = await db.Invoicings
                    .Include(b => b.Company)
                    .Where(i => i.CreatedAt.Month == currentMont && i.InvoicingStatus == status)
                    .ToListAsync();
                var invoicingsInfo = new List<InvoicingDTO>();

                foreach (var invoicing in invoicings)
                {
                    var invoicingInfo = new InvoicingDTO();

                    invoicingInfo.CompanyId = invoicing.CompanyId;
                    invoicingInfo.CompanyName = invoicing.Company.Name;
                    invoicingInfo.CompanyValue = invoicing.Company.Value == null ? 0 : invoicing.Company.Value;
                    invoicingInfo.CompanyCasinoValue = invoicing.Company.ValueForCasinoEmployees == null ? 0 : invoicing.Company.ValueForCasinoEmployees;
                    invoicingInfo.Type = invoicing.Company.CustomerType;
                    invoicingInfo.TotalActiveEmployees = invoicing.TotalActiveEmployees;
                    invoicingInfo.TotalInactiveEmployees = invoicing.TotalInactiveEmployees;
                    invoicingInfo.TotalActiveBranches = invoicing.TotalActiveBranches;
                    invoicingInfo.TotalInactiveBranches = invoicing.TotalInactiveBranches;
                    invoicingInfo.Invoiced = invoicing.Value;
                    invoicingInfo.InvoicedWithFormat = ("$ " + string.Format("{0:#,0}", invoicing.Value)).Replace(',', '.');
                    invoicingInfo.TotalActiveCasinoEmployees = invoicing.TotalActiveCasinoEmployees;
                    invoicingInfo.TotalInactiveCasinoEmployees = invoicing.TotalInactiveCasinoEmployees;
                    invoicingInfo.InvoicedByCasino = invoicing.ValuePerCasinoEmployee;
                    invoicingInfo.InvoicedByCasinoWithFormat = ("$ " + string.Format("{0:#,0}", invoicing.ValuePerCasinoEmployee)).Replace(',', '.');
                    invoicingInfo.HasCasino = invoicing.Company.HasCasino;
                    invoicingsInfo.Add(invoicingInfo);
                }

                return Ok(invoicingsInfo);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT: api/Invoicings/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutInvoicing(long id, Invoicing invoicing)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != invoicing.Id)
            {
                return BadRequest();
            }

            db.Entry(invoicing).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InvoicingExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Invoicings
        [ResponseType(typeof(Invoicing))]
        public async Task<IHttpActionResult> PostInvoicing(Invoicing request)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                Invoicing invoicing = new Invoicing
                {
                    CreatedAt = DateTime.Now,
                    CompanyId = request.CompanyId,
                    InvoicingType = request.InvoicingType,
                    TotalActiveBranches = request.TotalActiveBranches,
                    TotalActiveEmployees = request.TotalActiveEmployees,
                    TotalInactiveBranches = request.TotalInactiveBranches,
                    TotalInactiveEmployees = request.TotalInactiveEmployees,
                    Value = request.Value
                };

                db.Invoicings.Add(invoicing);

                await db.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpPost]
        [Route("PostInvoicingMassive")]
        public async Task<IHttpActionResult> PostInvoicingMassive(InvoicingRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                await invoicingRepository.PostInvoicingMassive(request);

                return Ok("Operacion realizada con exito");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        // DELETE: api/Invoicings/5
        [ResponseType(typeof(Invoicing))]
        public async Task<IHttpActionResult> DeleteInvoicing(long id)
        {
            Invoicing invoicing = await db.Invoicings.FindAsync(id);
            if (invoicing == null)
            {
                return NotFound();
            }

            db.Invoicings.Remove(invoicing);
            await db.SaveChangesAsync();

            return Ok(invoicing);
        }

        [HttpGet]
        [Route("VerifyIfMonthWasInvoiced")]
        public async Task<IHttpActionResult> VerifyIfMonthWasInvoiced()
        {
            try
            {
                bool result = false;
                var currentDate = DateTime.Now;
                var currentMont = currentDate.Month;
                var lastInvoicing = await db.Invoicings.Where(i => i.InvoicingStatus == InvoicingStatus.Invoiced).OrderByDescending(i => i.Id).FirstOrDefaultAsync();

                if (lastInvoicing != null)
                {
                    var lastDate = lastInvoicing.CreatedAt;
                    var lastMonth = lastDate.Month;

                    if (currentMont == lastMonth)
                    {
                        result = true;
                    }
                }

                return Ok(result);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("VerifyIfMonthWasPreInvoiced")]
        public async Task<IHttpActionResult> VerifyIfMonthWasPreInvoiced()
        {
            try
            {
                bool result = false;
                var currentDate = DateTime.Now;
                var currentMont = currentDate.Month;
                var lastInvoicing = await db.Invoicings.Where(i => i.InvoicingStatus == InvoicingStatus.PreInvoiced).OrderByDescending(i => i.Id).FirstOrDefaultAsync();

                if (lastInvoicing != null)
                {
                    var lastDate = lastInvoicing.CreatedAt;
                    var lastMonth = lastDate.Month;

                    if (currentMont == lastMonth)
                    {
                        result = true;
                    }
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool InvoicingExists(long id)
        {
            return db.Invoicings.Count(e => e.Id == id) > 0;
        }
    }
}