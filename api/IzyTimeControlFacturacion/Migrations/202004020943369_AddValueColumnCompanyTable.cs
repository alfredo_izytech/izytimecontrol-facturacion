﻿namespace IzyTimeControlFacturacion.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddValueColumnCompanyTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "Value", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "Value");
        }
    }
}
