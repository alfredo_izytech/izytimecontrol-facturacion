﻿using IzyTimeControlFacturacion.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IzyTimeControlFacturacion.DTO
{
    public class InvoicingDTO
    {
        public long CompanyId { get; set; }
        public string CompanyName { get; set; }
        public double? CompanyValue { get; set; }
        public double? CompanyCasinoValue { get; set; }
        public string CreatedAt { get; set; }
        public CustomerType Type { get; set; }
        public bool HasCasino { get; set; }
        public string ServerName { get; set; }
        public string Datasource { get; set; }
        public string DatabaseName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public long TotalActiveEmployees { get; set; }
        public long TotalInactiveEmployees { get; set; }
        public long TotalActiveCasinoEmployees { get; set; }
        public long TotalInactiveCasinoEmployees { get; set; }
        public long TotalActiveBranches { get; set; }
        public long TotalInactiveBranches { get; set; }
        public long Invoiced { get; set; }
        public string InvoicedWithFormat { get; set; }
        public long InvoicedByCasino { get; set; }
        public string InvoicedByCasinoWithFormat { get; set; }
    }
}