﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using IzyTimeControlFacturacion.Data;
using IzyTimeControlFacturacion.DTO;
using IzyTimeControlFacturacion.Models;
using IzyTimeControlFacturacion.Requests;

namespace IzyTimeControlFacturacion.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/companies")]
    public class CompaniesController : ApiController
    {
        private IzyTimeControlFacturacionContext db = new IzyTimeControlFacturacionContext();

        // GET: api/Companies
        public async Task<IHttpActionResult> GetCompanies()
        {
            try 
            {
                var data = await db.Companies.OrderByDescending(c => c.Id).ToListAsync();
                List<CompanyDTO> companies = new List<CompanyDTO>();
                data.ForEach(item =>
                {
                    CompanyDTO company = new CompanyDTO
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Value = item.Value,
                        ValueWithFormat = item.Value.ToString().Replace('.',','),
                        Type = item.CustomerType,
                        ActivationDate = item.ActivationDate,
                        HasCasino = item.HasCasino,
                        ValueForCasinoEmployees = item.ValueForCasinoEmployees,
                        ValueForCasinoEmployeesWithFormat = item.ValueForCasinoEmployees.ToString().Replace('.', ',')
                    };

                    companies.Add(company);
                });

                return Ok(companies);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET: api/Companies/5
        [ResponseType(typeof(Company))]
        public async Task<IHttpActionResult> GetCompany(long id)
        {
            Company company = await db.Companies.FindAsync(id);
            if (company == null)
            {
                return NotFound();
            }

            return Ok(company);
        }

        // PUT: api/Companies/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCompany(long id, UpdateCompanyRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != request.Id)
            {
                return BadRequest();
            }

            var company = db.Companies.Find(id);

            company.Value = request.Value;
            company.Name = request.Name;
            company.ActivationDate = request.ActivationDate;
            company.CustomerType = request.Type;
            company.HasCasino = request.HasCasino;
            company.ValueForCasinoEmployees = request.ValueForCasinoEmployees;

            db.Entry(company).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompanyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Companies
        [ResponseType(typeof(Company))]
        public async Task<IHttpActionResult> PostCompany(InsertCompanyRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Company company = new Company
            {
                Name = request.Name,
                Value = request.Value,
                CustomerType = request.Type,
                ActivationDate = request.ActivationDate,
                HasCasino = request.HasCasino,
                ValueForCasinoEmployees = request.ValueForCasinoEmployees
            };

            db.Companies.Add(company);
            await db.SaveChangesAsync();
            return Ok();
        }

        // DELETE: api/Companies/5
        [ResponseType(typeof(Company))]
        public async Task<IHttpActionResult> DeleteCompany(long id)
        {
            Company company = await db.Companies.FindAsync(id);
            if (company == null)
            {
                return NotFound();
            }

            db.Companies.Remove(company);
            await db.SaveChangesAsync();

            return Ok(company);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CompanyExists(long id)
        {
            return db.Companies.Count(e => e.Id == id) > 0;
        }
    }
}