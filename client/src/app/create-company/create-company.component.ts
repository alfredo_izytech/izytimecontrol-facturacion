import { Component, OnInit, Inject } from '@angular/core';
import { InvoicingType, Type } from '../core/models/invoicing.model';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CompanyService } from '../core/service/company.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CreateCompanyForm } from '../core/models/company.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-create-company',
  templateUrl: './create-company.component.html',
  styleUrls: ['./create-company.component.scss']
})
export class CreateCompanyComponent implements OnInit {
  data: CreateCompanyForm;
  formGroup: FormGroup;
  hasCasino = new FormControl(false);
  servers: any[] = [
    { Id: 1, Value: 'Servidor 1'},
    { Id: 2, Value: 'Servidor 2'},
    { Id: 3, Value: 'Servidor 3'}
  ];
  types: Type[] = [
    { Type: InvoicingType.ByActiveBranch, Value: 'Por sucursales activas' },
    { Type: InvoicingType.ByActiveEmployee, Value: 'Por empleados activos' },
    { Type: InvoicingType.ByFixValue, Value: 'Por valor fijo' },
    { Type: InvoicingType.SpecialBilling, Value: 'Facturación especial' }
  ];

  constructor(private formBuilder: FormBuilder,
              private service: CompanyService,
              public dialogRef: MatDialogRef<CreateCompanyComponent>,
              @Inject(MAT_DIALOG_DATA) data) {
}

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      activation_date: ['', [Validators.required]],
      id: ['', [Validators.required]],
      company: ['', [Validators.required]],
      type: ['', [Validators.required]],
      value: ['', [Validators.required]],
      has_casino: [false, [Validators.required]],
      casino_value: ['', [Validators.required]],
    });
  }

  createCompany() {
    const { activation_date, company, type, value, has_casino, casino_value} = this.formGroup.getRawValue();

    this.data = {
      ActivationDate: activation_date,
      Name: company,
      Value: value,
      Type: type,
      HasCasino: has_casino,
      ValueForCasinoEmployees: casino_value
    };

    Swal.fire({
      title: 'Ejecutando la operación, espere un momento',
      icon: 'info'
    });

    this.service.createCompany(this.data).subscribe((response) => {
      Swal.close();

      if (response.result) {
        Swal.fire({
          title: 'Operación exitosa',
          icon: 'success',
          timer: 5000
        });
        this.dialogRef.close();

      } else {
        Swal.fire({
          title: 'No se pudo realizar la operación',
          icon: 'error'
        });

        this.dialogRef.close();
      }
    });
  }
  onClose(): void {
    this.dialogRef.close();
  }
}
