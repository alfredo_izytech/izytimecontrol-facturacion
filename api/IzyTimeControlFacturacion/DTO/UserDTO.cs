﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IzyTimeControlFacturacion.DTO
{
    public class UserDTO
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Token { get; set; }
        public string Username { get; set; }
    }
}