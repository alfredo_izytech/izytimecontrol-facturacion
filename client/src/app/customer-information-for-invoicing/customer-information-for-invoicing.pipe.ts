import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customerInformationForInvoicing'
})
export class CustomerInformationForInvoicingPipe implements PipeTransform {

  monthNames = [
    'Enero',
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octubre',
    'Noviembre',
    'Diciembre'
  ];

  transform(value: any): string {
    const date = new Date();
    const response = value + ' ' + this.monthNames[date.getMonth()] + ' ' + date.getFullYear();
    return response;
  }

}
