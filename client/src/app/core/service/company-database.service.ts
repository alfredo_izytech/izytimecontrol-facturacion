import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { CreateDatabaseForm, UpdateDatabaseForm } from '../models/companydatabase.model';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CompanyDatabaseService {
  private readonly databaseCompanies = `api/companydatabases`;

  private httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) {}

  createDatabaseCompany(companyDatabase: CreateDatabaseForm) {
    return this.http.post<void>(this.databaseCompanies, companyDatabase, this.httpOptions).pipe(
      map(() => ({ result: true })),
      catchError((data) => of(
        {
          result: false,
          response: JSON.stringify(data.error)
        }
      ))
    );
  }

  getDatabaseCompanyById(companyId: number) {
    return this.http.get<UpdateDatabaseForm>(this.databaseCompanies + '/ByCompanyId?companyId=' + companyId, this.httpOptions);
  }

  updateDatabaseCompany(companyDatabase: UpdateDatabaseForm) {
    console.log(companyDatabase);

    return this.http.put<void>(this.databaseCompanies + '/' + companyDatabase.Id, companyDatabase, this.httpOptions).pipe(
      map(() => ({ result: true })),
      catchError((data) => of(
        {
          result: false,
          response: JSON.stringify(data.error)
        }
      ))
    );
  }
}
