export interface Invoicing {
    CreatedAt: string;
    CompanyId: number;
    CompanyName: string;
    CompanyValue: number;
    CompanyValueFormatted: string;
    CompanyCasinoValue: number;
    HasCasino: boolean;
    DataSource: string;
    Type: InvoicingType;
    TotalActiveBranches: number;
    TotalActiveEmployees: number;
    TotalInactiveBranches: number;
    TotalInactiveEmployees: number;
    TotalActiveCasinoEmployees: number;
    TotalInactiveCasinoEmployees: number;
    ServerName: string;
    ValuePerEmployee: number;
    ValuePerBranch: number;
    Invoiced: number;
    InvoicedWithFormat: string;
    InvoicedByCasino: number;
    InvoicedByCasinoWithFormat: string;
}

export enum InvoicingStatus {
    PreInvoiced = 0,
    Invoiced = 1
}

export enum InvoicingType {
    ByActiveEmployee = 0,
    ByActiveBranch = 1,
    ByFixValue = 2,
    SpecialBilling = 3
}

export interface Type {
    Type: InvoicingType;
    Value: string;
}