﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using IzyTimeControlFacturacion.Data;
using IzyTimeControlFacturacion.DTO;
using IzyTimeControlFacturacion.Models;
using IzyTimeControlFacturacion.Requests;

namespace IzyTimeControlFacturacion.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/companydatabases")]
    public class CompanyDatabasesController : ApiController
    {
        private IzyTimeControlFacturacionContext db = new IzyTimeControlFacturacionContext();

        // GET: api/CompanyDatabases
        public IQueryable<CompanyDatabase> GetCompanyDatabases()
        {
            return db.CompanyDatabases;
        }

        // GET: api/CompanyDatabases/5
        [ResponseType(typeof(CompanyDatabase))]
        public async Task<IHttpActionResult> GetCompanyDatabase(long id)
        {
            CompanyDatabase companyDatabase = await db.CompanyDatabases.FindAsync(id);
            if (companyDatabase == null)
            {
                return NotFound();
            }

            return Ok(companyDatabase);
        }

        [HttpGet]
        [Route("ByCompanyId")]
        public async Task<IHttpActionResult> GetCompanyDatabaseByCompanyID(long companyId)
        {
            var companyDatabase = await db.CompanyDatabases.Where(c => c.CompanyId == companyId).Select(c => new CompanyDatabaseDTO
            {
                Id = c.Id,
                CompanyId = c.CompanyId,
                DatabaseName = c.Name,
                ServerId = c.ServerId
            }).FirstOrDefaultAsync();

            return Ok(companyDatabase);
        }

        // PUT: api/CompanyDatabases/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCompanyDatabase(long id, UpdateCompanyDatabaseRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != request.Id)
            {
                return BadRequest();
            }

            try
            {
                var companyDatabase = db.CompanyDatabases.Find(request.Id);
                companyDatabase.CompanyId = request.CompanyId;
                companyDatabase.ServerId = request.ServerId;
                companyDatabase.Name = request.DatabaseName;

                db.Entry(companyDatabase).State = EntityState.Modified;

                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompanyDatabaseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok();
        }

        // POST: api/CompanyDatabases
        [ResponseType(typeof(CompanyDatabase))]
        public async Task<IHttpActionResult> PostCompanyDatabase(InsertCompanyDatabaseRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            CompanyDatabase companyDatabase = new CompanyDatabase
            {
                Name = request.Name,
                CompanyId = request.CompanyId,
                ServerId = request.ServerId
            };

            db.CompanyDatabases.Add(companyDatabase);
            await db.SaveChangesAsync();

            return Ok();
        }

        // DELETE: api/CompanyDatabases/5
        [ResponseType(typeof(CompanyDatabase))]
        public async Task<IHttpActionResult> DeleteCompanyDatabase(long id)
        {
            CompanyDatabase companyDatabase = await db.CompanyDatabases.FindAsync(id);
            if (companyDatabase == null)
            {
                return NotFound();
            }

            db.CompanyDatabases.Remove(companyDatabase);
            await db.SaveChangesAsync();

            return Ok(companyDatabase);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CompanyDatabaseExists(long id)
        {
            return db.CompanyDatabases.Count(e => e.Id == id) > 0;
        }
    }
}