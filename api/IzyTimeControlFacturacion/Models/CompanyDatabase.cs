﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IzyTimeControlFacturacion.Models
{
    public class CompanyDatabase
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; } 
        public long CompanyId { get; set; }
        public virtual Company Company { get; set; }
        public long ServerId { get; set; }
        public virtual Server Server { get; set; }
    }
}