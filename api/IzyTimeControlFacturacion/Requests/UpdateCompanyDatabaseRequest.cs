﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IzyTimeControlFacturacion.Requests
{
    public class UpdateCompanyDatabaseRequest
    {
        [Required]
        public long Id { get; set; }
        [Required]
        public string DatabaseName { get; set; }
        [Required]
        public long CompanyId { get; set; }
        [Required]
        public long ServerId { get; set; }
    }
}