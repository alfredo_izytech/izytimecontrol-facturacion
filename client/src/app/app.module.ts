import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { MaterialModule } from './material.module';
import { JwtInterceptor } from './core/helpers/jwt.interceptor';
import { UserService } from './core/service/user.service';
import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { NavigationComponent } from './navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
// tslint:disable-next-line: max-line-length
import { CustomerInformationForInvoicingComponent } from './customer-information-for-invoicing/customer-information-for-invoicing.component';
import { LayoutComponent } from './layout/layout.component';
import { ServerComponent } from './server/server.component';
import { CompanyComponent } from './company/company.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
// tslint:disable-next-line: max-line-length
import { CustomerInformationForInvoicingDirective } from './customer-information-for-invoicing/customer-information-for-invoicing.directive';
import { CustomerInformationForInvoicingPipe } from './customer-information-for-invoicing/customer-information-for-invoicing.pipe';
import { InvoicingComponent } from './invoicing/invoicing.component';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { CreateInvoicingComponent } from './create-invoicing/create-invoicing.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxCurrencyModule } from 'ngx-currency';
import { CommonModule, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { UpdateCompanyFormComponent } from './update-company-form/update-company-form.component';
import { MAT_DATE_LOCALE } from '@angular/material';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { ErrorInterceptor } from './core/helpers/error.interceptor';
import { CreateCompanyComponent } from './create-company/create-company.component';
import { DatabaseFormComponent } from './database-form/database-form.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavigationComponent,
    CustomerInformationForInvoicingComponent,
    LayoutComponent,
    ServerComponent,
    CompanyComponent,
    CustomerInformationForInvoicingDirective,
    CustomerInformationForInvoicingPipe,
    InvoicingComponent,
    CreateInvoicingComponent,
    UpdateCompanyFormComponent,
    CreateCompanyComponent,
    DatabaseFormComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(AppRoutes, {
      scrollPositionRestoration: 'enabled',
      useHash: true,
      preloadingStrategy: PreloadAllModules }),
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    MaterialModule,
    LayoutModule,
    FontAwesomeModule,
    SweetAlert2Module.forRoot(),
    NgbModule,
    NgxCurrencyModule,
    CommonModule
  ],
  providers: [
    UserService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    },
    {
      provide: MAT_DATE_LOCALE,
      useValue: 'es-CL'
    },
    { provide : LocationStrategy , useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
