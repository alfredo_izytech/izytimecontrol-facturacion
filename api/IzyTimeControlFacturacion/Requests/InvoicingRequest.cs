﻿using IzyTimeControlFacturacion.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IzyTimeControlFacturacion.Requests
{
    public class InvoicingRequest
    {
        [Required]
        public double Uf { get; set; }
        [Required]
        public InvoicingStatus Status { get; set; }
        [Required]
        public List<InvoiceInfo> Invoicings { get; set; }
        
        
    }

    public class InvoiceInfo
    {
        [Required]
        public long CompanyId { get; set; }
        [Required]
        public string CompanyName { get; set; }
        public double CompanyValue { get; set; }
        public double CompanyCasinoValue { get; set; }
        [Required]
        public InvoicingType Type { get; set; }
        [Required]
        public long TotalActiveBranches { get; set; }
        [Required]
        public long TotalActiveEmployees { get; set; }
        [Required]
        public long TotalInactiveBranches { get; set; }
        [Required]
        public long TotalInactiveEmployees { get; set; }
        [Required]
        public long TotalActiveCasinoEmployees { get; set; }
        [Required]
        public long TotalInactiveCasinoEmployees { get; set; }
        public long Invoiced { get; set; }
        public bool HasCasino { get; set; }
        public long InvoicedByCasino { get; set; }
    }
}