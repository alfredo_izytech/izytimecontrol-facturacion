import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ServerService } from '../core/service/server.service';
import { ServerList } from '../core/models/server.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CompanyDatabaseService } from '../core/service/company-database.service';
import { DatabaseFormType, CreateDatabaseForm, UpdateDatabaseForm } from '../core/models/companydatabase.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-database-form',
  templateUrl: './database-form.component.html',
  styleUrls: ['./database-form.component.scss']
})
export class DatabaseFormComponent implements OnInit {

  createForm: CreateDatabaseForm;
  companyId: number;
  formGroup: FormGroup;
  formType: DatabaseFormType;
  servers: ServerList[];
  updateForm: UpdateDatabaseForm;

  constructor(private companyDatabaseService: CompanyDatabaseService,
              private formBuilder: FormBuilder,
              private serverService: ServerService,
              public dialogRef: MatDialogRef<DatabaseFormComponent>,
              @Inject(MAT_DIALOG_DATA) data) {
                this.companyId = data.CompanyId;
                this.formGroup = this.formBuilder.group({
                  CompanyId: [this.companyId, Validators.required],
                  DatabaseName: ['', [Validators.required]],
                  ServerId: ['', [Validators.required]]
                });

                this.formType = DatabaseFormType.Create;
              }

  ngOnInit(): void {

    this.companyDatabaseService.getDatabaseCompanyById(this.companyId).subscribe(data => {
      console.log(data.Id);
      if (data != null) {
        this.formGroup = this.formBuilder.group({
          Id: [data.Id, [Validators.required]],
          CompanyId: [this.companyId, Validators.required],
          DatabaseName: [data.DatabaseName, [Validators.required]],
          ServerId: [data.ServerId, [Validators.required]]
        });

        this.formType = DatabaseFormType.Update;
      }
    });


    this.serverService.getAll().subscribe(data => {
      this.servers = data;
    });
  }

  onClose(): void {
    this.dialogRef.close();
  }

  saveChanges() {
    if (this.formType === DatabaseFormType.Create) {
      this.createDatabase();
    } else {
      this.updateDatabase();
    }
  }

  createDatabase() {
    const { companyId, databaseName, serverId } = this.formGroup.getRawValue();
    this.createForm = {
      CompanyId: companyId,
      DatabaseName: databaseName,
      ServerId: serverId
    };
    this.companyDatabaseService.createDatabaseCompany(this.createForm).subscribe((response) => {
      Swal.close();

      if (response.result) {
        Swal.fire({
          title: 'Operación exitosa',
          icon: 'success',
          timer: 5000
        });
        this.dialogRef.close();

      } else {
        Swal.fire({
          title: 'No se pudo realizar la operación',
          icon: 'error'
        });

        this.dialogRef.close();
      }
    });
  }

  updateDatabase() {
    const { Id, CompanyId, DatabaseName, ServerId  } = this.formGroup.getRawValue();
    this.updateForm = {
      Id,
      CompanyId,
      DatabaseName,
      ServerId
    };

    this.companyDatabaseService.updateDatabaseCompany(this.updateForm).subscribe((response) => {
      Swal.close();

      if (response.result) {
        Swal.fire({
          title: 'Operación exitosa',
          icon: 'success',
          timer: 5000
        });
        this.dialogRef.close();

      } else {
        Swal.fire({
          title: 'No se pudo realizar la operación',
          icon: 'error'
        });

        this.dialogRef.close();
      }
    });
  }
}

