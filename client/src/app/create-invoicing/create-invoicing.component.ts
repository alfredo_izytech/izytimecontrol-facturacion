import { Component, OnInit, Inject } from '@angular/core';
import Swal from 'sweetalert2';
import { CustomerInformationForInvoicingService } from '../customer-information-for-invoicing/customer-information-for-invoicing.service';
import { Invoicing, InvoicingStatus } from '../core/models/invoicing.model';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-invoicing',
  templateUrl: './create-invoicing.component.html',
  styleUrls: ['./create-invoicing.component.scss']
})
export class CreateInvoicingComponent implements OnInit {

  data: Invoicing[];
  formGroup: FormGroup;

  constructor(private service: CustomerInformationForInvoicingService,
              private formBuilder: FormBuilder,
              public dialogRef: MatDialogRef<CreateInvoicingComponent>,
              @Inject(MAT_DIALOG_DATA) data) {
      this.data = data.data;
    }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      uf: ['', [Validators.required]]
    });
  }

  saveInvoicing() {
    const { uf } = this.formGroup.getRawValue();

    Swal.fire({
      title: 'Ejecutando la operación, espere un momento',
      icon: 'info',
      showConfirmButton: false
    });

    this.service.savePreInvoicing(this.data, uf, InvoicingStatus.PreInvoiced).subscribe((response) => {
      Swal.close();

      if (response.result) {
        Swal.fire({
          title: 'Operación exitosa',
          icon: 'success',
          showConfirmButton: false,
          timer: 2000
        });

        this.dialogRef.close(true);
      } else {
        Swal.fire({
          title: 'No se pudo realizar la operación',
          icon: 'error',
          showConfirmButton: false,
          timer: 2000
        });
      }
    });
  }

  onClose(): void {
    this.dialogRef.close(false);
  }
}
