﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IzyTimeControlFacturacion.Models
{
    public class Server
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        public string Datasource { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public ICollection<CompanyDatabase> Databases { get; set; }
    }
}