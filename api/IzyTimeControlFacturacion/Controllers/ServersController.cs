﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using IzyTimeControlFacturacion.Data;
using IzyTimeControlFacturacion.DTO;
using IzyTimeControlFacturacion.Models;

namespace IzyTimeControlFacturacion.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/servers")]
    public class ServersController : ApiController
    {
        private IzyTimeControlFacturacionContext db = new IzyTimeControlFacturacionContext();

        // GET: api/Servers
        public async Task<IHttpActionResult> GetServers()
        {
            try
            {
                var servers = await db.Servers.Select(s => new ServerDTO
                {
                    Id = s.Id,
                    Datasource = s.Datasource,
                    Name = s.Name
                }).ToListAsync();

                return Ok(servers);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET: api/Servers/5
        [ResponseType(typeof(Server))]
        public async Task<IHttpActionResult> GetServer(long id)
        {
            Server server = await db.Servers.FindAsync(id);
            if (server == null)
            {
                return NotFound();
            }

            return Ok(server);
        }

        // PUT: api/Servers/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutServer(long id, Server server)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != server.Id)
            {
                return BadRequest();
            }

            db.Entry(server).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ServerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Servers
        [ResponseType(typeof(Server))]
        public async Task<IHttpActionResult> PostServer(Server server)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Servers.Add(server);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = server.Id }, server);
        }

        // DELETE: api/Servers/5
        [ResponseType(typeof(Server))]
        public async Task<IHttpActionResult> DeleteServer(long id)
        {
            Server server = await db.Servers.FindAsync(id);
            if (server == null)
            {
                return NotFound();
            }

            db.Servers.Remove(server);
            await db.SaveChangesAsync();

            return Ok(server);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ServerExists(long id)
        {
            return db.Servers.Count(e => e.Id == id) > 0;
        }
    }
}