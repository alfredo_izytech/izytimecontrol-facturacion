import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ServerList } from '../models/server.model';


@Injectable({
  providedIn: 'root'
})
export class ServerService {

  private readonly getServers = `api/servers`;

  constructor(private http: HttpClient) {}

  getAll(): Observable<ServerList[]> {
    return this.http.get<ServerList[]>(this.getServers);
  }
}
