﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IzyTimeControlFacturacion.DTO
{
    public class CompanyDatabaseDTO
    {
        public long Id { get; set; }
        public string DatabaseName { get; set; }
        public long CompanyId { get; set; }
        public long ServerId { get; set; }
    }
}