﻿using IzyTimeControlFacturacion.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace IzyTimeControlFacturacion.Requests
{
    public class InsertCompanyRequest
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public long Value { get; set; }
        [Required]
        public DateTime ActivationDate { get; set; }
        [Required]
        public CustomerType Type { get; set; }
        [Required]
        public bool HasCasino { get; set; }
        public double ValueForCasinoEmployees { get; set; }
    }
}