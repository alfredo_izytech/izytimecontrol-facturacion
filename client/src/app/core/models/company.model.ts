import { InvoicingType } from './invoicing.model';

export interface Company {
    Id: number;
    Name: string;
    Value: number;
    ValueWithFormat: string;
    ValueForCasinoEmployees: number;
    ValueForCasinoEmployeesWithFormat: string;
    HasCasino: boolean;
    ActivationDate: Date;
    Type: InvoicingType;
}

export interface CreateCompanyForm {
    Name: string;
    Value: number;
    ActivationDate: Date;
    Type: InvoicingType;
    ValueForCasinoEmployees: number;
    HasCasino: boolean;
}

export interface UpdateCompanyForm {
    Id: number;
    Name: string;
    Value: number;
    ActivationDate: Date;
    Type: InvoicingType;
    ValueForCasinoEmployees: number;
    HasCasino: boolean;
}
