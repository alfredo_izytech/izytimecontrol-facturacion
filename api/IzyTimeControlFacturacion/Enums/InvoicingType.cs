﻿namespace IzyTimeControlFacturacion.Enums
{
    public enum InvoicingType
    {
        ByActiveEmployee = 0,
        ByActiveBranch = 1,
        ByFixValue = 2,
        SpecialBilling = 3
    }
}