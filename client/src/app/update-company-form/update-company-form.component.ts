import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CompanyService } from '../core/service/company.service';
import Swal from 'sweetalert2';
import { Company, UpdateCompanyForm } from '../core/models/company.model';
import { Type, InvoicingType } from '../core/models/invoicing.model';

@Component({
  selector: 'app-update-company-form',
  templateUrl: './update-company-form.component.html',
  styleUrls: ['./update-company-form.component.scss']
})
export class UpdateCompanyFormComponent implements OnInit {
  data: UpdateCompanyForm;
  formGroup: FormGroup;
  selectedValue: InvoicingType;
  types: Type[] = [
    { Type: InvoicingType.ByActiveBranch, Value: 'Por sucursales activas' },
    { Type: InvoicingType.ByActiveEmployee, Value: 'Por empleados activos' },
    { Type: InvoicingType.SpecialBilling, Value: 'Facturación especial' }
  ];
  hasCasino = new FormControl(false);

  constructor(private formBuilder: FormBuilder,
              private service: CompanyService,
              public dialogRef: MatDialogRef<UpdateCompanyFormComponent>,
              @Inject(MAT_DIALOG_DATA) data) {
                this.data = data.data;
  }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      activation_date: [this.data.ActivationDate, [Validators.required]],
      id: [this.data.Id, [Validators.required]],
      company: [this.data.Name, [Validators.required]],
      type: [this.data.Type, [Validators.required]],
      value: [this.data.Value, [Validators.required]],
      has_casino: [this.data.HasCasino, [Validators.required]],
      casino_value: [this.data.ValueForCasinoEmployees, [Validators.required]],
    });

    this.hasCasino = new FormControl(this.data.HasCasino);
  }

  updateCompany() {
    const { activation_date, id, company,  type, value, has_casino, casino_value } = this.formGroup.getRawValue();
    const casinoValue = this.hasCasino.value ? casino_value : 0;
    console.log(this.hasCasino.value);
    console.log(casinoValue);
    this.data = {
      Id: id,
      Name: company,
      Value: value,
      ActivationDate: activation_date,
      Type: type,
      HasCasino: this.hasCasino.value,
      ValueForCasinoEmployees: casinoValue
    };

    Swal.fire({
      title: 'Ejecutando la operación, espere un momento',
      icon: 'info'
    });

    this.service.updateCompany(this.data).subscribe((response) => {
      Swal.close();

      if (response.result) {
        Swal.fire({
          title: 'Operación exitosa',
          icon: 'success',
          timer: 5000
        });
        this.dialogRef.close();

      } else {
        Swal.fire({
          title: 'No se pudo realizar la operación',
          icon: 'error'
        });

        this.dialogRef.close();
      }
    });
  }

  onClose(): void {
    this.dialogRef.close();
  }
}
