import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../service/auth.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    private readonly baseUrl = environment.apiBaseUrl;

    constructor(private authenticationService: AuthService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const reqUrl = this.baseUrl;
        // add authorization header with jwt token if available
        const currentUser = this.authenticationService.currentUserValue;
        if (currentUser && currentUser.Token) {
            request = request.clone({
                headers: request.headers.set(
                    'Authorization',
                    'Bearer ' + currentUser.Token
                ),
                url: reqUrl + '' + request.url
            });
            console.log( reqUrl + '' + request.url);
        }
        return next.handle(request);
    }
}
